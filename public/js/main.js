$(document).ready(function () {

    $.ajax({
        url: '/isLogged',
        type: 'GET',
        success: function (response) {
            if (response === 'true') {
                register_form.hide();
                more_info_form.show();
            }
        }
    });

    $("#birth_date").datepicker({
        dateFormat: 'yy-mm-dd',
        yearRange: "-100:+0",
        changeMonth: true,
        changeYear: true
    });

    let register_form = $("#register_form");
    let more_info_form = $("#more_info_form");
    let social = $("#social");

    register_form.validate();

    $("#to_more_info_form").click(function () {
        if (register_form.valid()) {
            let form_data = register_form.serialize();

            $.ajax({
                url: '/addNewMember',
                type: 'POST',
                dataType: 'json',
                data: {
                    "form_data": form_data
                },
                statusCode: {
                    200: function () {
                        register_form.hide();
                        more_info_form.fadeIn();
                    },
                    422: function () {
                        alert('Incorrect input');
                    },
                    409: function () {
                        alert('Email already in use');
                    }
                }
            });

        }
    });

   more_info_form.submit(function (e) {
       e.preventDefault();
       let formData = new FormData(this);

       $.ajax({
           url: '/member/update',
           type: 'POST',
           data: formData,
           statusCode: {
               200: function () {
                   $("#form_header").hide();
                   more_info_form.hide();
                   social.fadeIn();
               },
               422: function () {
                   alert('Incorrect input');
               }
           },
           cache: false,
           contentType: false,
           processData: false
       });
   });

});