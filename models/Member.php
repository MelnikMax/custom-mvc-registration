<?php

namespace models;

use components\DB;
use core\Model;

class Member extends Model
{
    public $table = 'members';

    public function __construct()
    {
        parent::__construct();
    }

    public function create(array $data)
    {
        $query = $this->db->prepare(
            'INSERT INTO members (first_name, last_name, birth_date, report_subject, country, phone_number, email) 
                      VALUES (?, ?, ?, ?, ?, ?, ?)'
        );

        $query->execute([
            $data['first_name'],
            $data['last_name'],
            $data['birth_date'],
            $data['report_subject'],
            $data['country'],
            $data['phone_number'],
            $data['email']
        ]);

        return $this->db->lastInsertId();
    }

    public function update(array $data)
    {
        $query = $this->db->prepare('UPDATE members SET company = ?, `position` = ?, about_me = ?, photo_name = ? WHERE id = ?');
        $query->execute([
            $data['company'],
            $data['position'],
            $data['about_me'],
            $data['photo_name'],
            $_SESSION['member_id']
        ]);
        session_unset();
    }

    public static function isEmailInUse($email)
    {
        $db = DB::getConnection();
        $query = $db->prepare('SELECT * FROM members WHERE email = ?');
        $query->execute([$email]);

        return $query->rowCount() != 0;
    }
}