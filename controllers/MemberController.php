<?php

namespace controllers;

use core\Controller;
use models\Member;

class MemberController extends Controller
{
    public function actionIndex()
    {
        $member = new Member();
        $members = $member->all();
        $this->render("members", ['members' => $members]);
        return true;
    }

    public function actionRegister()
    {
        $params = include(ROOT . '/config/params.php');
        $this->render("register", ['params' => $params]);
        return true;
    }

    public function actionIsLogged()
    {
        header('Content-type: application/json; charset=utf-8');

        if (empty($_SESSION['member_id'])) {
            echo json_encode("false");
        } else {
            echo json_encode("true");
        }

        return true;
    }

    private function validateCreating(array $data)
    {
        $date = date_parse($data['birth_date']);
        $phoneNumberPattern = '^[+][0-9] [(][0-9]{3}[)] [0-9]{3}[-][0-9]{4}^';

        if (
            ($date["error_count"] != 0 || !checkdate($date["month"], $date["day"], $date["year"])) ||
            (!preg_match($phoneNumberPattern, $data['phone'])) ||
            (strlen($data['first_name']) == 0 || strlen($data['first_name']) > 45) ||
            (strlen($data['last_name']) == 0 || strlen($data['last_name']) > 45) ||
            (strlen($data['report_subject']) == 0 || strlen($data['report_subject']) > 200) ||
            (strlen($data['country']) == 0 || strlen($data['country']) > 50)
        ) {
            http_response_code(422);
        } elseif (Member::isEmailInUse($data['email'])) {
            http_response_code(409);
        } else {
            return true;
        }

    }

    public function actionCreate()
    {
        $data = [];
        parse_str($_POST['form_data'], $data);
        header('Content-type: application/json; charset=utf-8');

        if (self::validateCreating($data) === true) {
            $member = new Member();
            $newMemberId = $member->create([
                'first_name' => $data['first_name'],
                'last_name' => $data['last_name'],
                'birth_date' => $data['birth_date'],
                'report_subject' => $data['report_subject'],
                'country' => $data['country'],
                'phone_number' => $data['phone'],
                'email' => $data['email']
            ]);
            $_SESSION['member_id'] = $newMemberId;
            http_response_code(200);

        } else {
            echo json_encode(self::validateCreating($data));
        }

        return true;
    }

    private function validateUpdating()
    {
        if (
            ($_FILES["photo"]["size"] < 200000) &&
            (
                $_FILES["photo"]["type"] == "image/gif" ||
                $_FILES["photo"]["type"] == "image/jpg" ||
                $_FILES["photo"]["type"] == "image/jpeg" ||
                $_FILES["photo"]["type"] == "image/png" ||
                empty($_FILES["photo"]["type"])
            ) &&
            (strlen($_POST['company']) < 45) &&
            (strlen($_POST['position']) < 45) &&
            (strlen($_POST['about_me']) < 300)
        ) {
            return true;
        } else {
            http_response_code(422);
        }
    }

    public function actionUpdate()
    {
        header('Content-type: application/json; charset=utf-8');

        if (self::validateUpdating() === true) {
            $img = $_FILES["photo"]["name"];
            $ext = strtolower(pathinfo($img, PATHINFO_EXTENSION));
            if (!empty($img)) {
                $randomName = uniqid() . ".$ext";
                move_uploaded_file($_FILES['photo']['tmp_name'], 'public/img/' . $randomName);
            } else {
                $randomName = 'no-photo.jpeg';
            }
            $member = new Member();
            $member->update([
                'company' => $_POST['company'],
                'position' => $_POST['position'],
                'about_me' => $_POST['about_me'],
                'photo_name' => $randomName
            ]);
            http_response_code(200);

        } else {
            echo json_encode(self::validateUpdating());
        }
        return true;
    }
}