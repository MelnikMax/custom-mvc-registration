<?php


namespace core;

use components\DB;
use PDO;

class Model
{
    protected $db;
    protected $table;

    public function __construct()
    {
        $this->db = DB::getConnection();
    }

    public function all()
    {
        $members = [];

        $result = $this->db->query("SELECT * FROM {$this->table}");
        $result->setFetchMode(PDO::FETCH_ASSOC);

        if ($result) {
            foreach ($result as $row) {
                $members[] = $row;
            }
        }

        return $members;
    }


}