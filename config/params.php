<?php

return [
    'host' => 'localhost',
    'dbname' => 'registration',
    'user' => 'root',
    'password' => '',
    'twitter_url' => 'https://www.wikipedia.org',
    'twitter_text' => 'Check out this Meetup with SoCal AngularJS!'
];