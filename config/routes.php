<?php

return [
    'isLogged' => 'member/isLogged',
    'members' => 'member/index',
    'addNewMember' => 'member/create',
    'member/update' => 'member/update',
    '' => 'member/register',
    'register' => 'member/register'
];
